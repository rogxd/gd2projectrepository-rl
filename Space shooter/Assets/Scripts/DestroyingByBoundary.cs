﻿using UnityEngine;
using System.Collections;

public class DestroyingByBoundary : MonoBehaviour {

	void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }
}